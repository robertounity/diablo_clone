﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

	private static PoolManager instance=null;
	public GameObject[] instances;
	public List<GameObject> poolInstances = new List<GameObject>();
	public int maxObjects;
	// Use this for initialization
	void Start () {
			PopulatePool ();
	}
	
	void Awake()
	{
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		}
		else 
		{
			instance = this;
		}
	}
	
	public static PoolManager Instance
	{
		get
		{
			return instance;
		}
	}

	private void PopulatePool()
	{
		for(int i=0;i<maxObjects;i++)
		{
			CreateInstance(0);
		}
	}

	public GameObject NextObject()
	{
		for (int i=0; i<maxObjects; i++) 
		{
			if(!poolInstances[i].activeSelf)
			{
				return poolInstances[i];
			}
		}
		return null;
	}


	private void CreateInstance(int index)
	{
		GameObject go = Instantiate (instances[index],Vector3.zero,Quaternion.identity) as GameObject;
		poolInstances.Add (go);
		go.SetActive (false);
	}
}
