﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

    public ScreenBase[] screens;

    private static HUDManager instance;

	public GameObject panelMP;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

    public static HUDManager Instance
    {
        get
        {
            return instance;
        }
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivatePanelMP()
	{
		panelMP.SetActive (true);
	}

    public void NotifyScoreChange(int score)
    {
        ((ScreenHUD)screens[(int)SCREENS.SCREEN_HUD]).NotifyScoreChange(score);
    }

    public void NotifyEnergyChange(int energy)
    {
        ((ScreenHUD)screens[(int)SCREENS.SCREEN_HUD]).NotifyEnergyChange(energy);
    }

    public void ShowWinScreen()
    {
        screens[(int)SCREENS.SCRREN_WIN].ShowScreen();
    }

    public void UseItem(int tipoItem)
    {
        LevelManager.Instance.playerActual.UseItem(tipoItem);
    }
}
