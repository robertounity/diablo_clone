using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class Player : EntityBase {

    public float speed;
    public float rotationSpeed;

    private Inventario inventario;
    private EntityStats stats;
    public GameObject bala;
    [HideInInspector]
    public Transform myTransform;

    public Animator animController;

    public GameObject puntoGuiaDisparo;
    public Camera camara;

	public ParticleSystem mySystem;

	public Vector3 newposition;
	public Quaternion newrotation;

	// Use this for initialization
	void Start () {
        myTransform = transform;
       // LevelManager.Instance.CrearEnemigos();
        stats = gameObject.GetComponent<EntityStats>();
        inventario = gameObject.GetComponent<Inventario>();
		LevelManager.Instance.playerActual = this;
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (gameObject.GetComponent<PhotonView> ().isMine) {

			if (Input.GetKeyDown (KeyCode.N)) {
				mySystem.Emit (100);
			}

			if (Input.GetKey (KeyCode.UpArrow)) {
				myTransform.position += myTransform.forward * speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				myTransform.position -= myTransform.forward * speed * Time.deltaTime;
			}

			if (Input.GetKey (KeyCode.LeftArrow)) {
				myTransform.eulerAngles -= new Vector3 (0, rotationSpeed * Time.deltaTime, 0);
			}

			if (Input.GetKey (KeyCode.RightArrow)) {
				myTransform.eulerAngles += new Vector3 (0, rotationSpeed * Time.deltaTime, 0);
			}

			if (Input.GetKeyDown (KeyCode.Space)) {
				gameObject.GetComponent<Rigidbody> ().AddForce (new Vector3 (0, 500, 0));
			}

			if (Input.GetKeyDown (KeyCode.K)) {
				camara.GetComponent<BloomOptimized> ().enabled = !camara.GetComponent<BloomOptimized> ().enabled;
			}

			if (Input.GetMouseButtonDown (0)) {
				//gameObject.GetComponent<AudioSource>().Play();
				SoundManager.Instance.PlaySFX((SFX.SFX_shoot));
				//GameObject go = Instantiate (bala, puntoGuiaDisparo.transform.position, myTransform.rotation) as GameObject;
				GameObject go = PoolManager.Instance.NextObject();
				go.transform.position = puntoGuiaDisparo.transform.position;
				go.transform.rotation = myTransform.rotation;
				go.GetComponent<Proyectil>().bIsPooled=true;
				go.GetComponent<Proyectil> ().parent = this;
				go.GetComponent<Proyectil> ().Start();
			}

			if (Input.GetMouseButtonDown (1)) {
				animController.SetBool ("Punch", true);
				animController.SetBool ("Idle", false);
			}
		}
		else 
		{
			transform.position = Vector3.Lerp(transform.position,newposition,Time.deltaTime*5);
			transform.rotation = Quaternion.Lerp(transform.rotation,newrotation,Time.deltaTime*5);
		}
        
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) 
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
		}
		else
		{
			newposition = (Vector3)stream.ReceiveNext();
			newrotation = (Quaternion)stream.ReceiveNext();
		}
	}


	[PunRPC] void EnviarAlgo(int algo)
	{

	}

    public void MostrarParticulasChocolocas()
    {
		gameObject.GetComponent<PhotonView> ().RPC ("EnviarAlgo",PhotonTargets.OthersBuffered,1);
    }

    public void RegresarAIdle()
    {
        animController.SetBool("Punch", false);
        animController.SetBool("Idle", true);
    }

    public void PausarJuego()
    {
        Time.timeScale = 0.5f;
    }

    public void DesPausarJuego()
    {
        Time.timeScale = 1;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Monedas"))
        {
            stats.puntos += other.gameObject.GetComponent<Monedas>().puntos;
            HUDManager.Instance.NotifyScoreChange(stats.puntos);
            Destroy(other.gameObject);
        }
        if (other.tag.Equals("Consumable"))
        {
            inventario.AddItem(other.gameObject.GetComponent<Consumables>());
            Destroy(other.gameObject);
        }
    }

    public void DestruyoEnemigo()
    {
        LevelManager.Instance.NotificacionDestruccionEnemigo();
    }

    public void Hitted(int damage)
    {
        stats.energia -= damage;
        HUDManager.Instance.NotifyEnergyChange(stats.energia);
    }

    public void UseItem(int tipoItem)
    {
        if((CONSUMABLE)tipoItem == CONSUMABLE.CONSUMABLE_ENERGY)
        {
            if (inventario.GetItem((CONSUMABLE)tipoItem) != null)
            {
                stats.energia += inventario.GetItem((CONSUMABLE)tipoItem).cantidadQueDa;
            }
        }
        if ((CONSUMABLE)tipoItem == CONSUMABLE.CONSUMABLE_MAGIC)
        {
            if (inventario.GetItem((CONSUMABLE)tipoItem) != null)
            {
                stats.magia += inventario.GetItem((CONSUMABLE)tipoItem).cantidadQueDa;
            }
        }
    }
}
