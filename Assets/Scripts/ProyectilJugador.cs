﻿using UnityEngine;
using System.Collections;

public class ProyectilJugador : Proyectil
{
    public Player parent;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag.Equals("Enemigo"))
        {
            other.gameObject.GetComponent<Enemigo>().Destruir();
            if (parent != null)
            {
                parent.DestruyoEnemigo();
            }
        }
    }
}
