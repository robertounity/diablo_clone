﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Camera camera = GetComponent<Camera>();
        float[] distances = new float[32];
        distances[9] = 1500;
        camera.layerCullDistances = distances;
	}
	
}
