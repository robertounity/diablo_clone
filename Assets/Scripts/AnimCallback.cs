﻿using UnityEngine;
using System.Collections;

public class AnimCallback : MonoBehaviour {

    public Player parent;

	public void MitadDelGolpe()
    {
        parent.MostrarParticulasChocolocas();
    }

    public void FinalDeAnimacion()
    {
        parent.RegresarAIdle();
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log("COLISOO "+other.gameObject.name);
        if (other.gameObject.tag.Equals("Enemigo"))
        {
            other.gameObject.GetComponent<Enemigo>().Destruir();
            if (parent != null)
            {
                parent.DestruyoEnemigo();
            }
        }
    }
}
