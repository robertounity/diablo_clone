﻿using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.MonoBehaviour {

	public string roomName = "mySuperCuartoDeHora";
	private RoomInfo[] roomslist;
	public GameObject myPlayer;
	void Awake()
	{
		if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated) {
			PhotonNetwork.ConnectUsingSettings ("0.9");
		}

		if (string.IsNullOrEmpty (PhotonNetwork.playerName)) {
			PhotonNetwork.playerName = "Ususario" + UnityEngine.Random.Range (1, 9999);
		}
	}
	public void CreateRoom()
	{
		PhotonNetwork.CreateRoom (this.roomName,new RoomOptions(){maxPlayers=2},null);
	}
	public void JoinRoom()
	{
		PhotonNetwork.JoinRoom (this.roomName);
	}
	public void OnJoinedRoom()
	{
		Debug.Log ("Enhorabuena, habeis entrado a un cuarto obscuro");
		PhotonNetwork.Instantiate (myPlayer.name,new Vector3(4,1,-20),Quaternion.Euler(Vector3.zero),0);
	}
	void OnConnectedToMaster()
	{
		Debug.Log ("AHORA SI SE PUEDE MASTER");
		HUDManager.Instance.ActivatePanelMP ();
	}

	void OnReceivedRoomListUpdate()
	{
		roomslist = PhotonNetwork.GetRoomList ();
		for (int i=0; i<roomslist.Length; i++) 
		{
			Debug.Log(roomslist[i].name);
		}
	}
}
