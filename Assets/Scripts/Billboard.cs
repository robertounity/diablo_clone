﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {

	public Camera myCamera;


	public enum Axis {up, down, left, right, forward, back};
	public bool reverseFace = false; 
	public Axis axis = Axis.up; 
	
	// return a direction based upon chosen axis
	public Vector3 GetAxis (Axis refAxis)
	{
		switch (refAxis)
		{
		case Axis.down:
			return Vector3.down; 
		case Axis.forward:
			return Vector3.forward; 
		case Axis.back:
			return Vector3.back; 
		case Axis.left:
			return Vector3.left; 
		case Axis.right:
			return Vector3.right; 
		}
		
		// default is Vector3.up
		return Vector3.up; 		
	}
		
	// Update is called once per frame
	void Update () 
	{
		if (myCamera == null) 
		{
			if(GameObject.Find ("MainCameraJugador0")!=null)
			{
				myCamera = GameObject.Find ("MainCameraJugador0").GetComponent<Camera>();
			}
		}
		else
		{
			Vector3 targetPos = transform.position + myCamera.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back) ;
			Vector3 targetOrientation = myCamera.transform.rotation * GetAxis(axis);
			transform.LookAt (targetPos, targetOrientation);
		}


	}
}
