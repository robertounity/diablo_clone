﻿using UnityEngine;
using System.Collections;

public class EnemyArea : MonoBehaviour {

	public float areaRadio=10;
	// Use this for initialization
	void Start () 
	{
		LevelManager.Instance.CrearEnemigosEnArea (transform.position,10,areaRadio,transform);
	}

}
