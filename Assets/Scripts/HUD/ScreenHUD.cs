﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenHUD : ScreenBase {

    public Text scoreText;
    public Text energyText;
	public Text nombreText;
    // Use this for initialization
    public override void Start ()
    {
        base.Start();
		nombreText.text = PlayerPrefs.GetString ("NombreJugador");
        ShowScreen();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public override void ShowScreen()
    {
        iTween.MoveTo(gameObject, iTween.Hash("y", 0, "time", 1));
    }

    public void NotifyScoreChange(int score)
    {
        scoreText.text = "Puntaje: " + score;
    }

    public void NotifyEnergyChange(int energy)
    {
        energyText.text = "Energia: " + energy;
    }
}
