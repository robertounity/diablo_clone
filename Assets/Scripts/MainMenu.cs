﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {


	public Text inputText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GoToGame()
    {
		PlayerPrefs.SetString("NombreJugador",inputText.text);
        //PlayerPrefs.SetInt("nivel",2);
        Application.LoadLevel(2);
        
    }

    public void GoToGameAsync()
    {
		GoToGame ();
       //StartCoroutine(LoadNewLevel());
    }

    IEnumerator LoadNewLevel()
    {
        //AsyncOperation async = Application.LoadLevelAdditiveAsync(1);
        AsyncOperation async = Application.LoadLevelAsync(1);
        yield return async;
        if (async.isDone)
        {
            Debug.Log("Loading complete");
        }
    }
}
