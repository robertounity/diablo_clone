﻿using UnityEngine;
using System.Collections;

public class Enemigo : EntityBase
{
    public  ENEMYS          type;
    public  GameObject      target;
    public  float           speed;
    public  bool            moverse;
    private NavMeshAgent    agent;
    public  GameObject      bala;
    private Transform       myTransform;
    [HideInInspector]
    public  Waypoints       rutaAPatrullar;
    public  Node            nodoActual;
    public bool siguiendoJugador = false;

    public void Start()
    {
       myTransform = transform;
       
       agent = gameObject.GetComponent<NavMeshAgent>();
       //InvokeRepeating("Disparar", Random.Range(1, 2), Random.Range(5, 8));

        if(type == ENEMYS.ENEMY_NORMAL)
        {
            rutaAPatrullar = LevelManager.Instance.GetRutaAPatrullar();
            nodoActual = rutaAPatrullar.GetFirstNode();
            transform.rotation = Quaternion.LookRotation(nodoActual.transform.position - transform.position);
        }
    }

    public void Disparar()
    {
        GameObject go = Instantiate(bala, myTransform.position + myTransform.forward, myTransform.rotation) as GameObject;
        go.GetComponent<Proyectil>().parent = this;
    }

	// Update is called once per frame
	void Update () 
    {
		if (target == null) 
		{
			if(LevelManager.Instance.GetActualPlayer()!=null)
			{
				target = LevelManager.Instance.GetActualPlayer().gameObject;
			}
			return;
		}


        if (type == ENEMYS.ENEMY_KAMIKAZE)
        {
            agent.SetDestination(target.transform.position);
        }
        if(type == ENEMYS.ENEMY_NORMAL)
        {
            NormalBehaviour();
        }
    }

    public void NormalBehaviour()
    {
        agent.SetDestination(nodoActual.transform.position);
        if (Vector3.SqrMagnitude(nodoActual.transform.position - transform.position) < 10)
        {
            nodoActual = rutaAPatrullar.GetNextNode(nodoActual.index);
        }

        if (Vector3.SqrMagnitude(target.transform.position - transform.position) < 40)
        {
            siguiendoJugador = true;
        }
        if (siguiendoJugador == true)
        {
            if (Vector3.SqrMagnitude(target.transform.position - transform.position) > 90)
            {
                siguiendoJugador = false;
            }
            agent.SetDestination(target.transform.position);
        }
    }

    void ManualMovement()
    {
        if (Vector3.SqrMagnitude(target.transform.position - transform.position) < 90)
        {
            moverse = true;
        }
        if (moverse)
        {
            if (Vector3.SqrMagnitude(target.transform.position - transform.position) > 8)
            {
                transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
                transform.position += transform.forward * speed * Time.deltaTime;
            }
        }
        if (Vector3.SqrMagnitude(target.transform.position - transform.position) > 30)
        {
            moverse = false;
        }

	}


    public void Destruir()
    {
        Destroy(transform.parent.gameObject);
    }
}
