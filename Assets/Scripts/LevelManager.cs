﻿using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;
using System;
using System.Text;

[System.Serializable]
public class LevelManager : MonoBehaviour {

    private static LevelManager instance;
    public int maxNumeroEnemigos;
    public int maxNumeroParedes;
    [HideInInspector]
    public int actualNumEnemigos;
    public GameObject[] enemigo;
    public GameObject wall;
    public Player playerActual;
    public WorldTile[] tiles;
    public int columnas;
    public int filas;
    public Waypoints[] waypointsDisponibles;

    private string PATH;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

    public static LevelManager Instance
    {
        get
        {
            return instance;
        }
    }

    // Use this for initialization
    void Start()
    {
        //CrearParedes();
        //CrearEscenario();
        CrearEnemigos();
        PATH = Application.dataPath + "/../testData/";
    }

    public Waypoints GetRutaAPatrullar()
    {
        return waypointsDisponibles[UnityEngine.Random.Range(0,waypointsDisponibles.Length-1)];
    }

    // Update is called once per frame
    void Update () {

	}

    private void OnGUI()
    {
        if (GUILayout.Button("SAVE"))
        {
            SerializeAndSave();
        }
        
        if (GUILayout.Button("LOAD"))
        {
            LoadAndDeserialize();
        }
    }

    private void SerializeAndSave()
    {
        StringBuilder sb = new StringBuilder();
        JsonWriter writer = new JsonWriter(sb);
        writer.WriteObjectStart();
        writer.WritePropertyName("numeroEnemigos");
        writer.Write(maxNumeroEnemigos);
        writer.WritePropertyName("numeroParedes");
        writer.Write(maxNumeroParedes);
        writer.WriteObjectEnd();

       // Debug.Log(data);

        if (!Directory.Exists(PATH))
        {
            Directory.CreateDirectory(PATH);
        }
        var streamWriter = new StreamWriter(PATH + "ejemplo" + ".json");
        streamWriter.Write(sb.ToString());
        streamWriter.Close();
        Debug.Log(PATH + "ejemplo" + ".json");
    }

    public void LoadAndDeserialize()
    {
        var streamReader = new StreamReader(PATH + "ejemplo" + ".json");
        string data = streamReader.ReadToEnd();
        streamReader.Close();
        JsonReader reader = new JsonReader(data);
		JsonData jsonEmp = JsonMapper.ToObject (reader);
		maxNumeroEnemigos = int.Parse(((IDictionary)jsonEmp["numeroEnemigos"]).ToString());
		maxNumeroParedes = int.Parse(((IDictionary)jsonEmp["numeroParedes"]).ToString());

    }

	public void CrearEnemigosEnArea(Vector3 puntoGuia,int maxNumeroEnemigosPorArea,float radioArea,Transform parent=null)
	{
		for (int i=0; i<maxNumeroEnemigosPorArea; i++) 
		{
			Vector3 posicionRandom = UnityEngine.Random.insideUnitSphere*radioArea;
			posicionRandom = new Vector3(posicionRandom.x,1,posicionRandom.z);
			GameObject go = Instantiate(enemigo[0], puntoGuia+posicionRandom,transform.rotation) as GameObject;
			if(parent!=null)
			{
				go.transform.parent = parent;
			}
		}
	}
		
		public void CrearEnemigos()
    {
        for(int i=0;i<maxNumeroEnemigos;i++)
        {
            if(UnityEngine.Random.Range(0,100) > 50)
            {
                Instantiate(enemigo[0], new Vector3(UnityEngine.Random.Range(-12, 12), 1, UnityEngine.Random.Range(-12, 12)),transform.rotation);
            }
            else
            {
                Instantiate(enemigo[1], new Vector3(UnityEngine.Random.Range(-12, 12), 1, UnityEngine.Random.Range(-12, 12)), transform.rotation);
            }
        }
    }

    public void CrearParedes()
    {
        for (int i = 0; i < maxNumeroParedes; i++)
        {
            Vector3 euler = transform.eulerAngles;
            euler.y = i * 90;
            GameObject go = Instantiate(wall, new Vector3(UnityEngine.Random.Range(-92, 92), 1, UnityEngine.Random.Range(-92, 92)), transform.rotation) as GameObject;
            go.transform.eulerAngles = euler;
        }
    }

    public void CrearEscenario()
    {
        for (int i = 0; i < columnas; i++)
        {
            for (int j = 0; j < filas; j++)
            {
                WorldTile tileACrear = TileValidoACrear(i,j);
                GameObject go = Instantiate(tileACrear, new Vector3(-63 + 52 * i, 0, 74 - (j * 54)), transform.rotation) as GameObject;

                //GameObject go = Instantiate(tileACrear, new Vector3(-63 + UnityEngine.Random.Range(72,100) * i, 0, 74 - (j * 54+(UnityEngine.Random.Range(20,30)))), transform.rotation) as GameObject;
            }
        }
    }

    public WorldTile TileValidoACrear(int i,int j)
    {
        WorldTile tileACrear = tiles[UnityEngine.Random.Range(0, tiles.Length)];
        if(i==0 && j==0)
        {
                if(tileACrear.exitType== TILE_EXIT.EXIT_NORTE || tileACrear.exitType== TILE_EXIT.EXIT_ORIENTE)
                {
                    return TileValidoACrear(i,j);
                }

         }
        else if (i == 0)
        {
            if (tileACrear.exitType == TILE_EXIT.EXIT_ORIENTE)
            {
                return TileValidoACrear(i,j);
            }

        }
        if (i == 0 && j == filas)
        {
            if (tileACrear.exitType == TILE_EXIT.EXIT_SUR || tileACrear.exitType == TILE_EXIT.EXIT_ORIENTE)
            {
                return TileValidoACrear(i, j);
            }

        }
        return tileACrear;
    }

    public void NotificacionDestruccionEnemigo()
    {
        actualNumEnemigos++;
        CheckWinConditions();
    }

    public void CheckWinConditions()
    {
        if(actualNumEnemigos>maxNumeroEnemigos)
        {
            //TODO: ganar
            Debug.Log("Win");
            HUDManager.Instance.ShowWinScreen();
        }
    }

    public Player GetActualPlayer()
    {
        return playerActual;
    }
}
