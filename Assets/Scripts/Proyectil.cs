﻿using UnityEngine;
using System.Collections;

public class Proyectil : EntityBase {

    public float  speed;
    public float  tiempoParaDesaparecer;
    public EntityBase parent;
	public bool bIsPooled=false;

	// Use this for initialization
	public void Start () 
    {
		gameObject.SetActive(true);
        Invoke("DestruirBala",tiempoParaDesaparecer);
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position += transform.forward*speed*Time.deltaTime;
	}

    void DestruirBala()
    {
		if (!bIsPooled) 
		{
			Destroy (gameObject);
		} 
		else 
		{
			gameObject.SetActive(false);
		}
    }

    
}
