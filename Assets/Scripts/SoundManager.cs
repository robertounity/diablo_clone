﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	private static SoundManager instance=null;
	public AudioSource audioBGM;
	public AudioSource audioSFX;
	[Range(0.0f,1.0f)]
	public float bgmVolume=1;
	public float sfxVolume=1;
	public AudioClip[] bgmAudioList;
	public AudioClip[] sfxAudioList;


	void Awake()
	{
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		}
		else 
		{
			instance = this;
		}
	}

	public static SoundManager Instance
	{
		get
		{
			return instance;
		}
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayBgm(BGM index)
	{
		audioBGM.clip = bgmAudioList[(int)index];
		audioBGM.loop = true;
		audioBGM.Play ();
	}

	public void PlaySFX(SFX index)
	{
		audioSFX.clip = sfxAudioList[(int)index];
		audioSFX.loop = false;
		audioSFX.Play ();

		//audioSFX.PlayOneShot (sfxAudioList[index]);
	}

	public void ChangeBGMVolume(float newVolume)
	{
		audioBGM.volume = newVolume;
	}

	public void ChangeSFXVolume(float newVolume)
	{
		audioSFX.volume = newVolume;
	}

	public void StopAll()
	{
		audioBGM.Stop ();
		audioSFX.Stop ();
	}

}
