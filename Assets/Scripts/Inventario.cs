﻿using UnityEngine;
using System.Collections;

public class Inventario : MonoBehaviour {

    public Consumables[] items;

	public void AddItem(Consumables item)
    {
        items[(int)item.tipo].numero += item.numero;

		///////
		Debug.Log (items[(int)item.tipo].numero);
	}

    public void UseItem(CONSUMABLE itemType)
    {
        items[(int)itemType].numero--;
    }

    public Consumables GetItem(CONSUMABLE itemType)
    {
        if(items[(int)itemType].numero>0)
        {
            return items[(int)itemType];
        }
        return null;
    }

    public void RemoveItem(Consumables item)
    {
        items[(int)item.tipo].numero--;
    }
}
