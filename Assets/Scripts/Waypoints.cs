﻿using UnityEngine;
using System.Collections;

public class Waypoints : MonoBehaviour {

    public Node[] nodes;

    public Node GetFirstNode()
    {
        return nodes[Random.Range(0, nodes.Length - 1)];
    }

    public Node GetNextNode(int index)
    {
        if((index+1) < nodes.Length-1)
        {
            return nodes[index+1];
        }
        else
        {
            return nodes[0];
        }
    }

}
