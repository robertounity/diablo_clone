﻿using UnityEngine;
using System.Collections;

public class ProyectilEnemigo : Proyectil
{
    public int damage;


    void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<Player>().Hitted(damage);
            Destroy(gameObject);
        }
    }
}
