﻿using UnityEngine;
using System.Collections;

public class HighScoreManager : MonoBehaviour {

	private static HighScoreManager instance;
	public string addScoreURL;
	public string getScoreURL;
	
	void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}
	}
	
	public static HighScoreManager Instance
	{
		get
		{
			return instance;
		}
	}

	public void SendPuntajesFromBtn()
	{
		SendPuntajes ("otro",666);
	}

	public void SendPuntajes(string nombreJugador="nombre",int puntaje=999)
	{
		StartCoroutine (EnviarPuntajes(nombreJugador,puntaje));
	}

	public void GetPuntajes()
	{
		StartCoroutine (RecibirPuntajes());
	}

	IEnumerator EnviarPuntajes(string name,int score)
	{
		string post_url = addScoreURL + "name=" + WWW.EscapeURL (name) + "&score=" + score;
		Debug.Log (post_url);
		WWW peticion = new WWW (post_url);
		yield return peticion;
		if (peticion.error != null) 
		{
			Debug.Log ("Error en la peticion "+peticion.error);
		}
	}

	IEnumerator RecibirPuntajes()
	{
		WWW peticion = new WWW (getScoreURL);
		yield return peticion;
		if (peticion.error != null) {
			Debug.Log ("Error en la peticion " + peticion.error);
		}
		else 
		{
			Debug.Log(peticion.text);
		}
	}
}
