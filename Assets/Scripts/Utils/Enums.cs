﻿using UnityEngine;
using System.Collections;

public enum SCREENS
{
    SCREEN_HUD,  //0
    SCRREN_WIN,  //1
}

public enum ENEMYS
{
    ENEMY_NORMAL,  //0
    ENEMY_KAMIKAZE,  //0
    ENEMY_BOSS,  //1
}

public enum TILE_TYPE
{
    TILE_BOSQUE,
    TILE_MAZMORRA,
}

public enum TILE_EXIT
{
    EXIT_SUR,
    EXIT_NORTE,
    EXIT_ORIENTE,
    EXIT_OCCIDENTE,
    EXIT_4_WAY,
    EXIT_2_WAY_VERTICAL,
    EXIT_2_WAY_HORIZONTAL,
}

public enum CONSUMABLE
{
    CONSUMABLE_ENERGY, //0
    CONSUMABLE_MAGIC, //1
}

public enum BGM
{
	BGM_Title,
	BGM_MainGame,
	BGM_End,
}

public enum SFX
{
	SFX_shoot,
	SFX_punch,
}