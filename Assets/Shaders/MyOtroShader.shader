﻿Shader "ELSUPERDUPERULTRACHEVERE/Texturizado" {
	Properties
	{
		_MainTex ("Texture",2D) = "white"{}
		_BumpMap ("Bumpmap",2D) = "bump" {}
		_RimColor ("RimColor",Color) = (1.0,0.0,0.0,1.0)
		_RimPower ("RimPower",Range(0.5,25.0)) = 2.0
	}
	
	SubShader
	{
		Tags {"RenderType" = "Opaque"}
		Cull Off
		CGPROGRAM
			#pragma surface surf Lambert
			struct Input
			{
			 	float2 uv_MainTex;
			 	float2 uv_BumpMap;
			 	float3 viewDir;
			 	float3 worldPos;
			};
			
			sampler2D _MainTex;
			sampler2D _BumpMap;
			float4 _RimColor;
			float _RimPower;
			
			void surf (Input IN,inout SurfaceOutput o)
			{
			  clip(frac((IN.worldPos.y+IN.worldPos.z*0.5)*7)-0.2);
			
			  o.Albedo = tex2D(_MainTex,IN.uv_MainTex).rgb;
			  o.Normal = UnpackNormal(tex2D(_BumpMap,IN.uv_BumpMap));
			  half rim = 1.0 - saturate(dot(normalize(IN.viewDir),o.Normal));
			  o.Emission = _RimColor.rgb*pow(rim,_RimPower);
			}
			
		ENDCG
	}
}
