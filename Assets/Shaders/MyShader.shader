﻿Shader "ELSUPERDUPERULTRACHEVERE/MyShader" {
 SubShader
 {
    Tags {"RenderType" = "Opaque" }
    CGPROGRAM
      #pragma surface coso Lambert
      struct Input
      {
        float4 color : COLOR;
      };
      
      void coso(Input IN, inout SurfaceOutput o)
      {
      	o.Albedo=1;
      }
    ENDCG
 }
}
